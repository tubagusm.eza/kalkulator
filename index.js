// Require function will allow you to call the module
const readline = require('readline');

// Initiate the readline to use stdin and stdout as input and output
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

console.clear();

//import function
const lingkaran = require('./lingkaran');
const persegi = require('./persegi');
const persegiPanjang = require('./persegiPanjang');
const segitiga = require('./segitiga');

// function luas(bangun) {
//     if (bangun == 'lingkaran') {
//         return lingkaran;
//     } else if (bangun == 'persegi') {
//         return persegi;
//     } else if (bangun == 'persegi panjang') {
//         return persegipanjang;
//     } else if ('bangun == segitiga') {
//         return segitiga;
//     } else {
//         console.log('Gaada Coy!');
//     }
// }

rl.question('Mau hitung luas apa nih? ', (bentuk) => {
    if (bentuk === 'lingkaran') {
        rl.question('Jari-jari= ', (r) => {
            console.log("Hasilnya adalah ", lingkaran.circle(r));
            rl.close();
        })
    } else if (bentuk === 'persegi') {
        rl.question('Sisi= ', (s) => {
            console.log("Hasilnya adalah ", persegi.rectangle(s));
            rl.close();
        })
    } else if (bentuk === 'persegi panjang') {
        rl.question('Panjang= ', (p) => {
            rl.question('Lebar= ', (l) => {
                console.log('Hasilnya adalah ', persegiPanjang.rectangular(p, l));
                rl.close();
            })
        })
    } else if (bentuk === 'segitiga') {
        rl.question('Alas= ', (a) => {
            rl.question('Tinggi= ', (b) => {
                console.log('Hasilnya adalah ', segitiga.triangle(a, b));
                rl.close();
            })
        })
    } else {
        console.log('Gaada Coy!');
        rl.close();
    }
});