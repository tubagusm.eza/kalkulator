function rectangle(s) {
  return s * s;
}

module.exports = {
  rectangle
};
